import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';

import {UsersService} from '../../shared/services/users.service';
import {UserModel} from '../../shared/services/models/user.model';
import {MessageModel} from '../../shared/services/models/message.model';
import {AuthService} from '../../shared/services/auth.service';

@Component({
  selector: 'ctn-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private form: FormGroup;
  message: MessageModel;

  constructor(
    private usersService: UsersService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
  ) { }

  private showMessage(message: MessageModel) {
    this.message = message;
    setTimeout(() => {
      this.message.text = '';
    }, 5000);
  }

  onSubmit() {
    const formData = this.form.value;
    this.usersService.getUserByEmail(formData.email)
      .subscribe((user: UserModel) => {
        if (user) {
          if (user.password === window.btoa(formData.password)) {
            this.authService.logIn(user);
            setTimeout(() => {
              this.router.navigate(['/system/billing']);
            }, 0);
          } else this.showMessage({
            text: 'Incorrect password!',
            type: 'danger',
          });
        } else this.showMessage({
          text: 'User not found!',
          type: 'danger'
        });
      });
  }
  ngOnInit() {
    this.message = new MessageModel('danger', '');
    this.route.queryParams
      .subscribe((params: Params) => {
        if (params['nowCanLogin']) {
          this.showMessage({
            text: 'Now You can Log In!',
            type: 'success',
          });
        } else if (params['accessDenied']) {
          this.showMessage({
            text: 'Need LogIn!',
            type: 'warning',
          })
        }
      });
    this.form = new FormGroup({
      'email': new FormControl(null, [
        Validators.required,
        Validators.email]),
      'password': new FormControl(null, [
        Validators.required,
        Validators.minLength(3)
      ]),
    });
  }

}
