import {Component, OnDestroy, OnInit} from '@angular/core';
import {BillingService} from '../shared/services/billing.service';
import {CategoriesService} from '../shared/services/categories.service';
import {EventsService} from '../shared/services/events.service';
import {Observable} from 'rxjs/Observable';
import {BillingModel} from '../shared/models/billing.model';
import {CategoryModel} from '../shared/models/category.model';
import {EventModel} from '../shared/models/event.model';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'ctn-planning',
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.scss']
})
export class PlanningComponent implements OnInit, OnDestroy {

  isLoaded = false;
  subscriptionForOnInit: Subscription;
  billing: BillingModel;
  categories: CategoryModel[] = [];
  events: EventModel[] = [];

  constructor(private billingService: BillingService,
              private categoryService: CategoriesService,
              private eventService: EventsService) {}


  getCategoryCost(category: CategoryModel): number  {
    const categoryEvents = this.events.filter(event => event.category === category.id && event.type === 'expenses');
    return categoryEvents.reduce((total, event) => {
      total += event.amount;
      return total;
    }, 0);
  }

  private getCategoryPercent(category: CategoryModel): number {
    const percent = (100 * this.getCategoryCost(category)) / category.capacity;
    return percent > 100 ? 100 : percent;
  }

  getCategoryPercentString(category: CategoryModel): string {
    return `${this.getCategoryPercent(category)}%`;
  }

  getCategoryColor(category: CategoryModel): string {
    const percent = this.getCategoryPercent(category);
    const className = percent < 60 ? 'success' : percent >= 100 ? 'danger' : 'warning';
    return className;
  }

  ngOnInit() {
    this.subscriptionForOnInit = Observable.combineLatest(
      this.billingService.getBill(),
      this.categoryService.getCategories(),
      this.eventService.getEvents(),
    ).subscribe((data: [BillingModel, CategoryModel[], any]) => {
      this.billing = data[0];
      this.categories = data[1];
      this.events = data[2];
      this.isLoaded = true;
    });
  }

  ngOnDestroy() {
    if (this.subscriptionForOnInit !== undefined) {
      this.subscriptionForOnInit.unsubscribe();
    }
  }

}
