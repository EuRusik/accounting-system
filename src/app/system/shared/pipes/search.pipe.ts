import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'ctnFilter'
})

export class SearchPipe implements PipeTransform {
  transform(items: any, value: string, field: string): any {
    if (items.length === 0 || !value) {
      return items;
    }
    return items.filter((item) => {
      const itemInstance = Object.assign({}, item);
      if (field === 'category') {
        itemInstance[field] = itemInstance['categoryName'];
      }
      return String(itemInstance[field]).toLowerCase().indexOf(value) !== -1;
    });
  }
}
