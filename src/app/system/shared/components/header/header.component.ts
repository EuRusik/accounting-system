import { Component, OnInit } from '@angular/core';
import {UserModel} from '../../../../shared/services/models/user.model';
import {AuthService} from '../../../../shared/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'ctn-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  date: Date = new Date();
  user: UserModel;

  constructor(private authService: AuthService, private router: Router) { }

  onLogOut() {
    this.authService.logOut();
    this.router.navigate(['/login']);
  }

  ngOnInit() {
    this.user = JSON.parse(window.localStorage.getItem('user'));
  }

}
