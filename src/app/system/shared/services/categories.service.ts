import {Api} from '../../../shared/core/api';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';

import {Injectable} from '@angular/core';
import {CategoryModel} from '../models/category.model';

@Injectable()
export class CategoriesService extends Api{
  constructor(protected http: Http) {
    super(http);
  }

  addCategory(category: CategoryModel): Observable<CategoryModel> {
    return this.post('categories', category);
  }

  getCategories(): Observable<CategoryModel[]> {
    return this.get('categories');
  }

  updateCategory(category: CategoryModel): Observable<CategoryModel> {
    return this.put(`categories/${category.id}`, category);
  }

  getCategoryById(id: number): Observable<CategoryModel> {
    return this.get(`categories/${id}`);
  }
}
