import {Api} from '../../../shared/core/api';
import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {EventModel} from '../models/event.model';

@Injectable()
export class EventsService extends Api {
  constructor(protected http: Http){
    super(http);
  }

  addEvent(event: EventModel): Observable<EventModel> {
    return this.post('events', event);
  }

  getEvents(): Observable<EventModel> {
    return this.get('events');
  }

  getEventById(id: string): Observable<EventModel>{
    return this.get(`events/${id}`);
  }

}
