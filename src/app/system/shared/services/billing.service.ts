import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {BillingModel} from '../models/billing.model';
import {Api} from '../../../shared/core/api';

@Injectable()
export class BillingService extends Api{

  constructor(protected http: Http) {
    super(http);
  }

  getBill(): Observable<BillingModel> {
    return this.get('bill');
  }

  updateBill(bill: BillingModel): Observable<BillingModel> {
    return this.put('bill', bill);
  }

  getCurrency(base: string = 'USD'): Observable<any> {
    return this.http.get(`https://api.fixer.io/latest?base=${base}`)
      .map((response: Response) => response.json());
  }

}
