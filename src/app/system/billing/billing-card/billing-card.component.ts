import {Component, Input, OnInit} from '@angular/core';
import {BillingModel} from '../../shared/models/billing.model';

@Component({
  selector: 'ctn-billing-card',
  templateUrl: './billing-card.component.html',
  styleUrls: ['./billing-card.component.scss']
})
export class BillingCardComponent implements OnInit {

  @Input() bill: BillingModel;
  @Input() currency: any;

  euro: number;
  rub: number;

  constructor() { }

  ngOnInit() {
    const  { rates } = this.currency;
    this.rub = rates['RUB'] * this.bill.value;
    this.euro = rates['EUR'] * this.bill.value;
  }

}
