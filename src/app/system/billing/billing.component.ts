import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import {BillingService} from '../shared/services/billing.service';
import {BillingModel} from '../shared/models/billing.model';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'ctn-billing',
  templateUrl: './billing.component.html',
  styleUrls: ['./billing.component.scss']
})
export class BillingComponent implements OnInit, OnDestroy {

  subscriptionForInit: Subscription;
  subscriptionForRefresh: Subscription;
  currency: any;
  bill: BillingModel;
  isLoaded = false;

  constructor(private billingService: BillingService) { }

  ngOnInit() {
    this.subscriptionForInit = Observable.combineLatest(
      this.billingService.getBill(),
      this.billingService.getCurrency(),
    ).subscribe((data: [BillingModel, any]) => {
      this.bill = data[0];
      this.currency = data[1];
      this.isLoaded = true;
    });
  }

  onRefresh() {
    this.isLoaded = false;
    this.subscriptionForRefresh = this.billingService.getCurrency()
      .subscribe((currency: any) => {
        this.currency = currency;
        this.isLoaded = true;
    });
  }

  ngOnDestroy() {
    this.subscriptionForInit.unsubscribe();
    if (this.subscriptionForRefresh !== undefined) {
        this.subscriptionForRefresh.unsubscribe();
    }
  }

}
