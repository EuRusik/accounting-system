import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ctn-currency-card',
  templateUrl: './currency-card.component.html',
  styleUrls: ['./currency-card.component.scss']
})
export class CurrencyCardComponent implements OnInit {
  @Input() currency: any;
  currencies: string[] = ['EUR', 'RUB'];

  constructor() { }

  ngOnInit() {
  }

}
