import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {EventsService} from '../../shared/services/events.service';
import {CategoriesService} from '../../shared/services/categories.service';
import {EventModel} from '../../shared/models/event.model';
import {CategoryModel} from '../../shared/models/category.model';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'ctn-log-detail',
  templateUrl: './log-detail.component.html',
  styleUrls: ['./log-detail.component.scss']
})
export class LogDetailComponent implements OnInit, OnDestroy {

  event: EventModel;
  category: CategoryModel;

  isLoaded = false;
  subscriptionForInit: Subscription;

  constructor(
    private route: ActivatedRoute,
    private eventService: EventsService,
    private categoryService: CategoriesService) { }

  ngOnInit() {
    this.route.params
      .mergeMap((params: Params) => this.eventService.getEventById(params['id']))
      .mergeMap((event: EventModel) => {
        this.event = event;
        return this.categoryService.getCategoryById(event.category);
    }).subscribe((category: CategoryModel) => {
      this.category = category;
      this.isLoaded = true;
    })
  }

  ngOnDestroy() {
    if (this.subscriptionForInit !== undefined) {
      this.subscriptionForInit.unsubscribe();
    }
  }

}
