import {Component, Input, OnInit} from '@angular/core';
import {CategoryModel} from '../../shared/models/category.model';

@Component({
  selector: 'ctn-log-events',
  templateUrl: './log-events.component.html',
  styleUrls: ['./log-events.component.scss']
})
export class LogEventsComponent implements OnInit {

  @Input() categories: CategoryModel[] = [];
  @Input() events: any[] = [];
  searchValue: string = '';
  searchPlaceholder = 'Amount';
  searchField = 'amount';

  constructor() { }

  getEventClass(event) {
    return {
      'label': true,
      'label-danger': event.type === 'expenses',
      'label-success': event.type === 'income',
    }
  }

  changeOption(field: string) {
    const namesMap = {
      amount: 'Amount',
      date: 'Date',
      category: 'Category',
      type: 'Type'
    };
    this.searchPlaceholder = namesMap[field];
    this.searchField = field;
  }

  ngOnInit() {
    this.events.forEach((event) => {
      event.categoryName = this.categories.find((category) => category.id === event.category).name;
    });
  }

}
