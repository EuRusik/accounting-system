import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ctn-log-chart',
  templateUrl: './log-chart.component.html',
  styleUrls: ['./log-chart.component.scss']
})
export class LogChartComponent implements OnInit {
  @Input() data;

  constructor() { }

  ngOnInit() {
  }

}
