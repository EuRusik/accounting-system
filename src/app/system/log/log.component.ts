import {Component, OnDestroy, OnInit} from '@angular/core';
import {CategoriesService} from '../shared/services/categories.service';
import {EventsService} from '../shared/services/events.service';
import {Observable} from 'rxjs/Observable';
import {CategoryModel} from '../shared/models/category.model';
import {EventModel} from '../shared/models/event.model';
import {Subscription} from 'rxjs/Subscription';
import * as moment from 'moment';

@Component({
  selector: 'ctn-log',
  templateUrl: './log.component.html',
  styleUrls: ['./log.component.scss']
})
export class LogComponent implements OnInit, OnDestroy {

  isLoaded = false;
  subscriptionForInit: Subscription;
  categories: CategoryModel[] = [];
  events: any[] = [];
  filterEvents: any[] = [];
  chartData = [];
  isFilterShowing = false;

  constructor(private categoryService: CategoriesService, private eventService: EventsService) { }

  calculateChartData(): void {
    this.chartData = [];
    this.categories.forEach((category) => {
      const categoryEvents = this.filterEvents.filter((event) => event.category === category.id && event.type === 'expenses');
      this.chartData.push({
        name: category.name,
        value: categoryEvents.reduce((total, event) => {
          total += event.amount;
          return total;
        }, 0),
      });
    });
  }

  private toggleFilter(dir: boolean) {
    this.isFilterShowing = dir;
  }

  openFilter() {
    this.toggleFilter(true);
  }

  onFilterApply(filterData) {
    this.toggleFilter(false);
    this.setOriginalEvents();

    const startPeriod = moment().startOf(filterData.period).startOf('d');
    const endPeriod = moment().endOf(filterData.period).endOf('d');

    this.filterEvents = this.filterEvents.filter((event) => {
      return filterData.types.indexOf(event.type) !== -1;
    }).filter((event) => {
      return filterData.categories.indexOf(event.category.toString()) !== -1;
    }).filter((event) => {
      const momentDate = moment(event.date, 'DD.MM.YYYY HH:mm:ss');
      return momentDate.isBetween(startPeriod, endPeriod);
    });
    this.calculateChartData();
  }

  onFilterCancel() {
    this.toggleFilter(false);
    this.setOriginalEvents();
    this.calculateChartData();
  }

  private setOriginalEvents() {
    this.filterEvents = this.events.slice();
  }

  ngOnInit() {
    this.subscriptionForInit = Observable.combineLatest(
      this.categoryService.getCategories(),
      this.eventService.getEvents(),
    ).subscribe((data: [CategoryModel[], any]) => {
      this.categories = data[0];
      this.events = data[1];
      this.setOriginalEvents();
      this.calculateChartData();
      this.isLoaded = true;
    });
  }

  ngOnDestroy() {
    if (this.subscriptionForInit !== undefined) {
      this.subscriptionForInit.unsubscribe();
    }
  }

}
