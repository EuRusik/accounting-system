import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SystemComponent} from './system.component';
import {BillingComponent} from './billing/billing.component';
import {LogComponent} from './log/log.component';
import {PlanningComponent} from './planning/planning.component';
import {EntryComponent} from './entry/entry.component';
import {LogDetailComponent} from './log/log-detail/log-detail.component';
import {AuthGuard} from '../shared/auth.guard';

const routes: Routes = [
  {path: '', component: SystemComponent, canActivate: [AuthGuard], children: [
      {path: 'billing', component: BillingComponent},
      {path: 'log', component: LogComponent},
      {path: 'planning', component: PlanningComponent},
      {path: 'entry', component: EntryComponent},
      {path: 'log/:id', component: LogDetailComponent},
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class SystemRoutingModule { }
