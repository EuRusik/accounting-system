import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {SystemRoutingModule} from './system-routing.module';
import { SystemComponent } from './system.component';
import { BillingComponent } from './billing/billing.component';
import { LogComponent } from './log/log.component';
import { PlanningComponent } from './planning/planning.component';
import {EntryComponent} from './entry/entry.component';
import { SidebarComponent } from './shared/components/sidebar/sidebar.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { DropdownDirective } from './shared/directives/dropdown.directive';
import { BillingCardComponent } from './billing/billing-card/billing-card.component';
import { CurrencyCardComponent } from './billing/currency-card/currency-card.component';
import {BillingService} from './shared/services/billing.service';
import {MomentPipe} from './shared/pipes/moment.pipe';
import { AddEventComponent } from './entry/add-event/add-event.component';
import { AddCategoryComponent } from './entry/add-category/add-category.component';
import { EditCategoryComponent } from './entry/edit-category/edit-category.component';
import {CategoriesService} from './shared/services/categories.service';
import {EventsService} from './shared/services/events.service';
import { LogChartComponent } from './log/log-chart/log-chart.component';
import { LogEventsComponent } from './log/log-events/log-events.component';
import { LogDetailComponent } from './log/log-detail/log-detail.component';
import { LogFilterComponent } from './log/log-filter/log-filter.component';
import {SearchPipe} from './shared/pipes/search.pipe';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    SystemRoutingModule
  ],
  declarations: [
    SystemComponent,
    BillingComponent,
    EntryComponent,
    LogComponent,
    PlanningComponent,
    SidebarComponent,
    HeaderComponent,
    DropdownDirective,
    BillingCardComponent,
    CurrencyCardComponent,
    MomentPipe,
    AddEventComponent,
    AddCategoryComponent,
    EditCategoryComponent,
    LogChartComponent,
    LogEventsComponent,
    LogDetailComponent,
    LogFilterComponent,
    SearchPipe,
  ],
  providers: [
    BillingService,
    CategoriesService,
    EventsService
  ]
})
export class SystemModule { }
