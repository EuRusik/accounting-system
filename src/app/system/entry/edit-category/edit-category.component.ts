import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgForm} from '@angular/forms';
import {CategoryModel} from '../../shared/models/category.model';
import {CategoriesService} from '../../shared/services/categories.service';
import {MessageModel} from '../../../shared/services/models/message.model';

@Component({
  selector: 'ctn-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit {

  @Input() categories: CategoryModel[] = [];
  @Output() onCategoryEdit = new EventEmitter<CategoryModel>();

  currentCategoryId = 1;
  currentCategory: CategoryModel;
  message: MessageModel;

  constructor(private categoryService: CategoriesService) { }

  onSubmit(form: NgForm) {
    let {capacity, name} = form.value;
    if (capacity < 0) {
      capacity *= - 1;
    }
    const category = new CategoryModel(name, capacity, Number(this.currentCategoryId));
    this.categoryService.updateCategory(category).subscribe((category: CategoryModel) => {
      this.onCategoryEdit.emit(category);
      this.message.text = 'Category was success edited!';
      window.setTimeout(() => this.message.text = '', 5000);
    });
  }

  onCategoryChange() {
    this.currentCategory = this.categories.find(category => category.id === Number(this.currentCategoryId));
  }

  ngOnInit() {
    this.message = new MessageModel('success', '');
    this.onCategoryChange();
  }

}
