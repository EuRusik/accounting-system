import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {CategoryModel} from '../../shared/models/category.model';
import {NgForm} from '@angular/forms';
import {EventModel} from '../../shared/models/event.model';
import * as moment from 'moment';
import {EventsService} from '../../shared/services/events.service';
import {BillingService} from '../../shared/services/billing.service';
import {BillingModel} from '../../shared/models/billing.model';
import {MessageModel} from '../../../shared/services/models/message.model';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'ctn-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent implements OnInit, OnDestroy {

  subscriptionForGetBilling: Subscription;
  subscriptionForUpdateBilling: Subscription;

  @Input() categories: CategoryModel[] = [];
  message: MessageModel;
  types = [
    {
      type: 'income',
      label: 'Income'
    },
    {
      type: 'expenses',
      label: 'Expenses'
    },
  ];

  constructor(private eventsService: EventsService, private billingService: BillingService) { }

  onSubmit(form: NgForm) {
    let {amount, description, category, type} = form.value;
    if(amount < 0) {
      amount *= -1;
    }
    const event = new EventModel(type, amount, Number(category), moment().format('DD.MM.YYYY HH:mm:ss'), description);
    this.subscriptionForGetBilling = this.billingService.getBill()
      .subscribe((billing: BillingModel) => {
        let value = 0;
        console.log(type);
        if (type === 'expenses') {
          if (amount > billing.value) {
            this.showMessage(`There is not enough money in your account. You need ${amount - billing.value}`);
            return;
          } else {
            value = billing.value - amount;
          }
        } else {
          value = billing.value + amount;
        }
        this.subscriptionForUpdateBilling = this.billingService.updateBill({value, currency: billing.currency})
          .mergeMap(() => this.eventsService.addEvent(event))
          .subscribe(() => {
            form.setValue({
              amount: 0,
              description: '',
              category: 1,
              type: 'expenses'
            });
          });
      });
    // this.eventsService.addEvent(event);
  }

  private showMessage(text: string) {
    this.message.text = text;
    window.setTimeout(() => this.message.text = '', 5000);
  }

  ngOnInit() {
    this.message = new MessageModel('danger', '');
  }

  ngOnDestroy() {
    if (this.subscriptionForGetBilling !== undefined) {
      this.subscriptionForGetBilling.unsubscribe();
    }
    if (this.subscriptionForUpdateBilling !== undefined) {
      this.subscriptionForUpdateBilling.unsubscribe();
    }
  }

}
