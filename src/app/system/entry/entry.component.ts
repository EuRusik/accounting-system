import {Component, Input, OnInit} from '@angular/core';
import {CategoryModel} from '../shared/models/category.model';
import {CategoriesService} from '../shared/services/categories.service';

@Component({
  selector: 'ctn-entry',
  templateUrl: './entry.component.html',
  styleUrls: ['./entry.component.scss']
})
export class EntryComponent implements OnInit {

  categories: CategoryModel[] = [];
  isLoaded = false;

  constructor(private categoryService: CategoriesService) { }

  ngOnInit() {
    this.categoryService.getCategories().subscribe((categories: CategoryModel[]) => {
      this.categories = categories;
      this.isLoaded = true;
    });
  }

  newCategoryAdded(category: CategoryModel) {
    this.categories.push(category);
  }

  categoryWasEdited(category: CategoryModel) {
    const index = this.categories.findIndex(c => c.id === category.id);
    this.categories[index] = category;
  }
}
