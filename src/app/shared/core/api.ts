import {Http, Response} from '@angular/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class Api {
  private baseURL = 'http://localhost:3000';

  constructor(protected http: Http) {}

  private getURL(url: string = ''): string {
    return `${this.baseURL}/${url}`;
  }

  public get(url: string = ''): Observable<any>  {
    return this.http.get(this.getURL(url))
      .map((response: Response) => response.json());
  }

  public post(url: string = '', data: any = {}): Observable<any>  {
    return this.http.post(this.getURL(url), data)
      .map((response: Response) => response.json());
  }

  public put(url: string = '', data: any = {}): Observable<any>  {
    return this.http.put(this.getURL(url), data)
      .map((response: Response) => response.json());
  }
}
