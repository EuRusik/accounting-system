import { Injectable } from '@angular/core';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {UserModel} from './models/user.model';
import {Api} from '../core/api';

@Injectable()
export class UsersService extends Api {

  constructor(protected http: Http) {
    super(http);
  }

  getUserByEmail(email: string): Observable<UserModel> {
    return this.get(`users?email=${email}`)
      .map((user: UserModel) => {
            if (Array.isArray(user)) {
              return user[0];
            } return false;
          });
  }

  createNewUser(user: UserModel): Observable<UserModel> {
    return this.post('users', user);
  }

}
