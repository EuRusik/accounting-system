import {Injectable} from '@angular/core';

@Injectable()
export class AuthService {

  private isAuth = false;
  logIn(user) {
    if (user && user !== null && typeof user === 'object') {
      window.localStorage.setItem('user', JSON.stringify(user));
    }
    this.isAuth = true;
  }
  logOut() {
    this.isAuth = false;
    window.localStorage.clear();
  }
  isLoggedIn(): boolean {
    return this.isAuth;
  }

}
